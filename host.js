/////////////////////////////////////////////////////////////////////////////////////////////
//
// host
//
//    Library that provides information about the host environment.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var host;

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Host Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function Host(data) {
    if (data && !data.runtime)              throw new Error(host.ERROR_INVALID_HOST_DATA, "The host data is missing the mandatory 'runtime' property.");
    if (data && !data.runtimeVersion)       throw new Error(host.ERROR_INVALID_HOST_DATA, "The host data is missing the mandatory 'runtimeVersion' property.");
    if (data && !data.platform)             throw new Error(host.ERROR_INVALID_HOST_DATA, "The host data is missing the mandatory 'platform' property.");
    if (data && !data.platformVersion)      throw new Error(host.ERROR_INVALID_HOST_DATA, "The host data is missing the mandatory 'platformVersion' property.");
    if (data && !data.platformArchitecture) throw new Error(host.ERROR_INVALID_HOST_DATA, "The host data is missing the mandatory 'platformArchitecture' property.");

    var runtime = data? data.runtime : null;
    Object.defineProperty(this, "runtime", {
        get: function () {
            if (!runtime) {
                runtime = detectRuntime();
            }
            return runtime;
        },
        enumerable: true
    });

    var runtimeVersion = data? data.runtimeVersion : null;
    Object.defineProperty(this, "runtimeVersion", {
        get: function () {
            //runtimeVersion detection is dependent on platform detection
            if (!runtimeVersion) {
                runtimeVersion = detectRuntimeVersion();
            }
            return runtimeVersion;
        },
        enumerable: true
    });

    var platform = data? data.platform : null;
    Object.defineProperty(this, "platform", {
        get: function () {
            if (!platform) {
                platform = detectPlatform();
            }
            return platform;
        },
        enumerable: true
    });

    var platformVersion = data? data.platformVersion : null;
    Object.defineProperty(this, "platformVersion", {
        get: function () {
            //platformVersion detection is dependent on platform detection
            if (!platformVersion) {
                platformVersion = detectPlatformVersion();
            }
            return platformVersion;
        },
        enumerable: true
    });

    var platformArchitecture = data? data.platformArchitecture : null;
    Object.defineProperty(this, "platformArchitecture", {
        get: function () {
            if (!platformArchitecture) {
                platformArchitecture = detectPlatformArchitecture();
            }
            return platformArchitecture;
        },
        enumerable: true
    });

    function detectRuntime() {
        try {
            var gui = require("nw.gui");
            if (gui) {
                return host.RUNTIME_NWJS;
            }
        } catch (e) {
            //TEST FAILED, CHECK OTHERS
        }
        //IS TESSEL A RUNTIME OR A PLATFORM??
        /*try {
            var tessel = require('tessel');
            return host.RUNTIME_TESSEL;
            } catch(e) {
            }*/
        if (typeof window !== "undefined" && window.process && window.process.type === "renderer") {
            return host.RUNTIME_ELECTRON;
        }
        if (typeof window !== "undefined" && window.__adobe_cep__) {
            return host.RUNTIME_ADOBECEP;
        }
        if (new Function("try {return this===global;}catch(e){return false;}") && typeof process === "object" && typeof process.versions === "object" && typeof process.versions.node !== "undefined") {
            return host.RUNTIME_NODEJS
        }
        if (typeof window !== "undefined" && new Function("try {return this===window;}catch(e){ return false;}")) {
            //Thank you! specific browser detection
            //http://stackoverflow.com/questions/9847580/how-to-detect-safari-chrome-ie-firefox-and-opera-browser

            // Opera 8.0+
            var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
            if (isOpera) {
                return host.RUNTIME_BROWSER_OPERA;
            }

            // Firefox 1.0+
            var isFirefox = typeof InstallTrigger !== 'undefined';
            if (isFirefox) {
                return host.RUNTIME_BROWSER_FIREFOX;
            }

            // Safari 3.0+ "[object HTMLElementConstructor]"
            var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 || (function (p) {
                    return p.toString() === "[object SafariRemoteNotification]";
                })(!window['safari'] || safari.pushNotification);
            if (isSafari) {
                return host.RUNTIME_BROWSER_SAFARI;
            }

            // Internet Explorer 6-11
            var isIE = /*@cc_on!@*/false || !!document.documentMode;
            if (isIE) {
                return host.RUNTIME_BROWSER_INTERNET_EXPLORER;
            }

            // Edge 20+
            var isEdge = !isIE && !!window.StyleMedia;
            if (isEdge) {
                return host.RUNTIME_BROWSER_EDGE;
            }

            // Chrome 1+
            var isChrome = !!window.chrome && !!window.chrome.webstore;
            if (isChrome) {
                return host.RUNTIME_BROWSER_CHROME;
            }

            // Blink engine detection
            var isBlink = (isChrome || isOpera) && !!window.CSS;
            if (isBlink) {
                return host.RUNTIME_BROWSER_BLINK;
            }

            //no match, unknown browser
            return host.RUNTIME_BROWSER;
        }
        if (typeof JSXGlobals !== "undefined") {
            return host.RUNTIME_ADOBEJSX;
        }
        return host.RUNTIME_UNKNOWN;
    }

    function detectRuntimeVersion() {
        if (!runtime) {
            runtime = detectRuntime();
        }
        if (!platform) {
            platform = detectPlatform();
        }

        //the version could have been set by running the detectPlatform function
        if (runtimeVersion) {
            return runtimeVersion;
        }

        //for browsers the runtime version is aquired in by running the detectPlatform function.
        switch (runtime) {
            case host.RUNTIME_NODEJS:
                return process.versions.node;
            case host.RUNTIME_NWJS:
                return process.versions["node-webkit"];
            case host.RUNTIME_ELECTRON:
                return process.versions["electron"];
            case host.RUNTIME_ADOBECEP:
                if (typeof window.__adobe_cep__.getHostEnvironment == "function") {
                    var env = JSON.parse(window.__adobe_cep__.getHostEnvironment());
                    return env.appId + " " + env.appVersion;
                }
        }

        return host.VERSION_UNKNOWN;
    }

    function detectPlatform() {
        if (!runtime) {
            runtime = detectRuntime();
        }

        if (runtime == host.RUNTIME_BROWSER ||
            runtime == host.RUNTIME_BROWSER_BLINK ||
            runtime == host.RUNTIME_BROWSER_CHROME ||
            runtime == host.RUNTIME_BROWSER_FIREFOX ||
            runtime == host.RUNTIME_BROWSER_SAFARI ||
            runtime == host.RUNTIME_BROWSER_INTERNET_EXPLORER ||
            runtime == host.RUNTIME_BROWSER_EDGE ||
            runtime == host.RUNTIME_BROWSER_OPERA) {
            /*if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
                return host.PLATFORM_IOS;
                }
                if (navigator.appVersion.indexOf("Win")!=-1) {
                return host.PLATFORM_WINDOWS;
                }
                if (navigator.appVersion.indexOf("Mac")!=-1) {
                return host.PLATFORM_MACOS;
                }
                if (navigator.appVersion.indexOf("Linux")!=-1) {
                return host.PLATFORM_LINUX;
                }*/

            //Thank You! Platform detection code using navigator string
            //http://stackoverflow.com/questions/9514179/how-to-find-the-operating-system-version-using-javascript

            // browser
            var nVer = navigator.appVersion;
            var nAgt = navigator.userAgent;
            var browser = navigator.appName;
            var browserVersion = "" + parseFloat(navigator.appVersion);
            var majorVersion = parseInt(navigator.appVersion, 10);
            var nameOffset, verOffset, ix;

            // Opera
            if ((verOffset = nAgt.indexOf("Opera")) != -1) {
                browser = "Opera";
                browserVersion = nAgt.substring(verOffset + 6);
                if ((verOffset = nAgt.indexOf("Version")) != -1) {
                    browserVersion = nAgt.substring(verOffset + 8);
                }
            }
            // Opera Next
            if ((verOffset = nAgt.indexOf("OPR")) != -1) {
                browser = "Opera";
                browserVersion = nAgt.substring(verOffset + 4);
            }
            // Edge
            else if ((verOffset = nAgt.indexOf("Edge")) != -1) {
                browser = "Microsoft Edge";
                browserVersion = nAgt.substring(verOffset + 5);
            }
            // MSIE
            else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
                browser = "Microsoft Internet Explorer";
                browserVersion = nAgt.substring(verOffset + 5);
            }
            // Chrome
            else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
                browser = "Chrome";
                browserVersion = nAgt.substring(verOffset + 7);
            }
            // Safari
            else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
                browser = "Safari";
                browserVersion = nAgt.substring(verOffset + 7);
                if ((verOffset = nAgt.indexOf("Version")) != -1) {
                    browserVersion = nAgt.substring(verOffset + 8);
                }
            }
            // Firefox
            else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
                browser = "Firefox";
                browserVersion = nAgt.substring(verOffset + 8);
            }
            // MSIE 11+
            else if (nAgt.indexOf("Trident/") != -1) {
                browser = "Microsoft Internet Explorer";
                browserVersion = nAgt.substring(nAgt.indexOf("rv:") + 3);
            }
            // Other browsers
            else if ((nameOffset = nAgt.lastIndexOf(" ") + 1) < (verOffset = nAgt.lastIndexOf("/"))) {
                browser = nAgt.substring(nameOffset, verOffset);
                browserVersion = nAgt.substring(verOffset + 1);
                if (browser.toLowerCase() == browser.toUpperCase()) {
                    browser = navigator.appName;
                }
            }
            // trim the browserVersion string
            if ((ix = browserVersion.indexOf(";")) != -1) browserVersion = browserVersion.substring(0, ix);
            if ((ix = browserVersion.indexOf(" ")) != -1) browserVersion = browserVersion.substring(0, ix);
            if ((ix = browserVersion.indexOf(")")) != -1) browserVersion = browserVersion.substring(0, ix);

            majorVersion = parseInt("" + browserVersion, 10);
            if (isNaN(majorVersion)) {
                browserVersion = "" + parseFloat(navigator.appVersion);
                majorVersion = parseInt(navigator.appVersion, 10);
            }

            // mobile browserVersion
            //var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(nVer);

            // system
            var os = host.PLATFORM_UNKNOWN;
            var clientStrings = [
                {s: "Windows 10", r: /(Windows 10.0|Windows NT 10.0)/},
                {s: "Windows 8.1", r: /(Windows 8.1|Windows NT 6.3)/},
                {s: "Windows 8", r: /(Windows 8|Windows NT 6.2)/},
                {s: "Windows 7", r: /(Windows 7|Windows NT 6.1)/},
                {s: "Windows Vista", r: /Windows NT 6.0/},
                {s: "Windows Server 2003", r: /Windows NT 5.2/},
                {s: "Windows XP", r: /(Windows NT 5.1|Windows XP)/},
                {s: "Windows 2000", r: /(Windows NT 5.0|Windows 2000)/},
                {s: "Windows ME", r: /(Win 9x 4.90|Windows ME)/},
                {s: "Windows 98", r: /(Windows 98|Win98)/},
                {s: "Windows 95", r: /(Windows 95|Win95|Windows_95)/},
                {s: "Windows NT 4.0", r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/},
                {s: "Windows CE", r: /Windows CE/},
                {s: "Windows 3.11", r: /Win16/},
                {s: "Android", r: /Android/},
                {s: "Open BSD", r: /OpenBSD/},
                {s: "Sun OS", r: /SunOS/},
                {s: "Linux", r: /(Linux|X11)/},
                {s: "iOS", r: /(iPhone|iPad|iPod)/},
                {s: "Mac OS X", r: /(Mac OS X|macOS)/},
                {s: "Mac OS", r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/},
                {s: "QNX", r: /QNX/},
                {s: "UNIX", r: /UNIX/},
                {s: "BeOS", r: /BeOS/},
                {s: "OS/2", r: /OS\/2/},
                {s: "Search Bot", r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/}
            ];
            for (var id in clientStrings) {
                var cs = clientStrings[id];
                if (cs.r.test(nAgt)) {
                    os = cs.s;
                    break;
                }
            }

            var osVersion = host.VERSION_UNKNOWN;
            switch (os) {
                case "Windows 10":
                case "Windows 8.1":
                case "Windows 8":
                case "Windows 7":
                case "Windows Vista":
                case "Windows Server 2003":
                case "Windows XP":
                case "Windows 2000":
                case "Windows ME":
                case "Windows 98":
                case "Windows 95":
                case "Windows NT 4.0":
                case "Windows CE":
                case "Windows 3.11":
                    osVersion = /Windows (.*)/.exec(os)[1];
                    os = host.PLATFORM_WINDOWS;
                    break;
                case "Open BSD":
                    os = host.PLATFORM_FREEBSD;
                    break;
                case "Sun OS":
                    os = host.PLATFORM_SOLARIS;
                    break;
                case "Linux":
                    os = host.PLATFORM_LINUX;
                    break;
                case "Mac OS X":
                    osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                    osVersion = osVersion.replace(/\_/gi, ".");
                case "Mac OS":
                    os = host.PLATFORM_MACOS;
                    break;
                case "Android":
                    os = host.PLATFORM_ANDROID;
                    osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                    break;
                case "iOS":
                    os = host.PLATFORM_IOS;
                    osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                    osVersion = osVersion[1] + "." + osVersion[2] + "." + (osVersion[3] | 0);
                    break;
            }

            runtimeVersion = browserVersion;
            platformVersion = osVersion;
            return os;
        }
        else if (typeof process !== "undefined" && typeof process.platform != "undefined") {
            switch (process.platform) {
                case "darwin":
                    return host.PLATFORM_MACOS;
                    break;
                case "freebsd":
                    return host.PLATFORM_FREEBSD;
                    break;
                case "sunos":
                    return host.PLATFORM_SOLARIS;
                    break;
                case "linux":
                    try {
                        //Read platformVersion files for various linux distros

                        //first test lsb-release file
                        var fs = require("fs");
                        try {
                            var data = fs.readFileSync("/etc/lsb-release");

                            /*

                                DISTRIB_ID=Ubuntu
                                DISTRIB_RELEASE=12.04
                                DISTRIB_CODENAME=precise
                                DISTRIB_DESCRIPTION="Ubuntu 12.04.3 LTS"

                                */
                            var release = null;
                            data.toString().split("\n").forEach(function (line, index, arr) {
                                if (index === arr.length - 1 && line === "") {
                                    return;
                                }

                                //index, line
                                var kv = line.split("=");
                                if (kv.length > 1) {
                                    switch (kv[0]) {
                                        case "DISTRIB_ID":
                                            switch (kv[1]) {
                                                case "Ubuntu":
                                                    release = host.PLATFORM_LINUX_UBUNTU;
                                                    break;
                                                case "Chakra":
                                                    release = host.PLATFORM_LINUX_CHAKRA;
                                                    break;
                                                case "IYCC":
                                                    release = host.PLATFORM_LINUX_IYCC;
                                                    break;
                                                case "Mint":
                                                    release = host.PLATFORM_LINUX_MINT;
                                                    break;
                                            }
                                            break;
                                        case "DISTRIB_RELEASE":
                                            platformVersion = kv[1];
                                            break;
                                    }
                                }
                            });

                            if (release) {
                                return release;
                            }
                        }
                        catch (e) {
                            //file probably does not exist
                        }

                        try {
                            var data = fs.readFileSync("/etc/os-release");

                            var release = null;
                            data.toString().split("\n").forEach(function (line, index, arr) {
                                if (index === arr.length - 1 && line === "") {
                                    return;
                                }

                                //index, line
                                var kv = line.split("=");
                                if (kv.length > 1) {
                                    switch (kv[0]) {
                                        case "PRETTY_NAME":
                                            //remove quotes
                                            kv[1] = kv[1].replace(/^"(.+(?="$))"$/, '$1');
                                            if (kv[1].length >= 10 && kv[1].substr(0, 10) == "ReadyNASOS") {
                                                release = host.PLATFORM_LINUX_READYNASOS;
                                                if (kv[1].length > 10) {
                                                    platformVersion = kv[1].substr(11);
                                                }
                                            }
                                            break;
                                    }
                                }
                            });

                            if (release) {
                                return release;
                            }
                        } catch (e) {
                            //file probably does not exist
                        }

                        //TODO - implement more
                        //http://linuxmafia.com/faq/Admin/release-files.html
                    }
                    catch (e) {
                        //ignore error, return unknown linux
                        console.error("PLATFORM DETECTION ERROR: " + e);
                    }
                    return host.PLATFORM_LINUX;
                    break;
                case "win32":
                    return host.PLATFORM_WINDOWS;
                    break;
            }
        }
        else if (runtime == host.RUNTIME_ADOBEJSX) {
            //TODO
            //system.osName sometimes returns an empty string
            //system.osVersion ex. returns "Build Number: 7601 Service Pack 1"
        }

        //unable to determine platform
        return host.PLATFORM_UNKNOWN;
    }

    function detectPlatformVersion() {
        if (!platform) {
            platform = detectPlatform();
        }

        var os = null;
        try {
            os = require("os");
        }
        catch (e) {

        }
        if (typeof process !== "undefined" && os != null) {
            switch (platform) {
                /*case host.PLATFORM_LINUX_DEBIAN:
                    case host.PLATFORM_LINUX_FEDORA:
                    case host.PLATFORM_LINUX_GENTOO:
                    case host.PLATFORM_LINUX_MANDRAKE:
                    case host.PLATFORM_LINUX_REDHAT:
                    case host.PLATFORM_LINUX_SLACKWARE:
                    case host.PLATFORM_LINUX_SUSE:
                    case host.PLATFORM_LINUX_UBUNTU:
                    return;*/
                case host.PLATFORM_WINDOWS:
                    //see specific windows table
                    //https://msdn.microsoft.com/en-us/library/windows/desktop/ms724832(v=vs.85).aspx
                    //minimum supported is Windows 7 -> 6.1
                    var v = "";
                    var r = os.release();
                    if (isOfMinimumVersion(r, "6.1")) {
                        v = "7";
                    }
                    if (isOfMinimumVersion(r, "6.2")) {
                        v = "8";
                    }
                    if (isOfMinimumVersion(r, "6.3")) {
                        v = "8.1";
                    }
                    if (isOfMinimumVersion(r, "10.0")) {
                        v = "10";
                    }
                    //last known entry
                    if (isOfMinimumVersion(r, "16.4.0")) {
                        v = "> 10.12.2";
                    }
                    return v;
                case host.PLATFORM_MACOS:
                    //see specific osx table
                    //https://en.wikipedia.org/wiki/Darwin_%28operating_system%29#Release_history
                    //minimum supported is 10.7 -> 11.0.0
                    var v = "";
                    var r = os.release();
                    if (isOfMinimumVersion(r, "11.0.0")) {
                        v = "10.7";
                    }
                    if (isOfMinimumVersion(r, "11.4.2")) {
                        v = "10.7.5";
                    }
                    if (isOfMinimumVersion(r, "12.0.0")) {
                        v = "10.8";
                    }
                    if (isOfMinimumVersion(r, "12.6.0")) {
                        v = "10.8.5";
                    }
                    if (isOfMinimumVersion(r, "13.0.0")) {
                        v = "10.9";
                    }
                    if (isOfMinimumVersion(r, "13.4.0")) {
                        v = "10.9.5";
                    }
                    if (isOfMinimumVersion(r, "14.0.0")) {
                        v = "10.10";
                    }
                    if (isOfMinimumVersion(r, "14.5.0")) {
                        v = "10.10.5";
                    }
                    if (isOfMinimumVersion(r, "15.0.0")) {
                        v = "10.11";
                    }
                    if (isOfMinimumVersion(r, "15.6.0")) {
                        v = "10.11.6";
                    }
                    if (isOfMinimumVersion(r, "16.0.0")) {
                        v = "10.12";
                    }
                    if (isOfMinimumVersion(r, "16.1.0")) {
                        v = "10.12.1";
                    }
                    if (isOfMinimumVersion(r, "16.3.0")) {
                        v = "10.12.2";
                    }
                    //last known entry
                    if (isOfMinimumVersion(r, "16.4.0")) {
                        v = "> 10.12.2";
                    }
                    return v;
                default:
                    //Return default release value (often does not reflect the public OS platformVersion).
                    return os.release();
            }
        }
        else {
            return host.VERSION_UNKNOWN;
        }
    }

    function detectPlatformArchitecture() {
        if (typeof process !== "undefined" && typeof process.arch != "undefined") {
            //'arm', 'arm64', 'ia32', 'mips', 'mipsel', 'ppc', 'ppc64', 's390', 's390x', 'x32', 'x64', and 'x86'
            switch (process.arch) {
                case "arm":
                    return host.PLATFORM_ARCHITECTURE_ARM;
                case "arm64":
                    return host.PLATFORM_ARCHITECTURE_ARM64;
                case "x86":
                    return host.PLATFORM_ARCHITECTURE_X86;
                case "x64":
                    return host.PLATFORM_ARCHITECTURE_X64;
                default:
                    return host.PLATFORM_ARCHITECTURE_UNKNOWN;
            }
        }
        else {
            return host.PLATFORM_ARCHITECTURE_UNKNOWN;
        }
    }

    //thanks! https://www.packtpub.com/books/content/platform-detection-your-nwjs-app
    function isOfMinimumVersion(version, minimumVersion) {
        if (version == null || minimumVersion == null) {
            return false;
        }
        else if (version == null) {
            return true;
        }
        var actualVersionPieces = ("" + version).split("."),
            pieces = minimumVersion.split("."),
            numberOfPieces = pieces.length;

        for (var i = 0; i < numberOfPieces; i++) {
            var piece = parseInt(pieces[i], 10),
                actualPiece = parseInt(actualVersionPieces[i], 10);

            if (typeof actualPiece === "undefined") {
                break;
            }
            else if (actualPiece > piece) {
                break;
            }
            else if (actualPiece === piece) {
                continue;
            }
            else {
                return false;
            }
        }

        return true;
    }
}
Host.prototype.isPlatformLinuxFamily = function () {
    switch (this.platform) {
        case host.PLATFORM_LINUX:
        case host.PLATFORM_LINUX_CHAKRA:
        case host.PLATFORM_LINUX_DEBIAN:
        case host.PLATFORM_LINUX_FEDORA:
        case host.PLATFORM_LINUX_GENTOO:
        case host.PLATFORM_LINUX_IYCC:
        case host.PLATFORM_LINUX_MANDRAKE:
        case host.PLATFORM_LINUX_MINT:
        case host.PLATFORM_LINUX_READYNASOS:
        case host.PLATFORM_LINUX_REDHAT:
        case host.PLATFORM_LINUX_SLACKWARE:
        case host.PLATFORM_LINUX_SUSE:
        case host.PLATFORM_LINUX_UBUNTU:
            return true;
        default:
            return false;
    }
}
Host.prototype.isRuntimeBrowserFamily = function () {
    switch (this.runtime) {
        case host.RUNTIME_BROWSER:
        case host.RUNTIME_BROWSER_BLINK:
        case host.RUNTIME_BROWSER_CHROME:
        case host.RUNTIME_BROWSER_EDGE:
        case host.RUNTIME_BROWSER_FIREFOX:
        case host.RUNTIME_BROWSER_INTERNET_EXPLORER:
        case host.RUNTIME_BROWSER_OPERA:
        case host.RUNTIME_BROWSER_SAFARI:
        case host.RUNTIME_NWJS:
        case host.RUNTIME_ELECTRON:
        case host.RUNTIME_ADOBECEP:
            return true;
        default:
            return false;
    }
}
Host.prototype.isRuntimeNodeFamily = function () {
    switch (this.runtime) {
        case host.RUNTIME_NODEJS:
        case host.RUNTIME_NWJS:
        case host.RUNTIME_ELECTRON:
        case host.RUNTIME_ADOBECEP:
            return true;
        default:
            return false;
    }
}
Host.prototype.parse = function(data) {
    return new Host(data);
}
Host.prototype.toJSON = function() {
    return {
        "runtime" :                this.runtime,
        "runtimeVersion" :         this.runtimeVersion,
        "platform" :               this.platform,
        "platformVersion" :        this.platformVersion,
        "platformArchitecture" :   this.platformArchitecture
    }
}
Host.prototype.RUNTIME_BROWSER = "browser";
Host.prototype.RUNTIME_BROWSER_BLINK = "browser blink";
Host.prototype.RUNTIME_BROWSER_CHROME = "browser chrome";
Host.prototype.RUNTIME_BROWSER_FIREFOX = "browser firefox";
Host.prototype.RUNTIME_BROWSER_SAFARI = "browser safari";
Host.prototype.RUNTIME_BROWSER_INTERNET_EXPLORER = "browser internet-explorer";
Host.prototype.RUNTIME_BROWSER_EDGE = "browser edge";
Host.prototype.RUNTIME_BROWSER_OPERA = "browser opera";
Host.prototype.RUNTIME_NWJS = "nw-js";
Host.prototype.RUNTIME_NODEJS = "node-js";
Host.prototype.RUNTIME_ELECTRON = "electron";
Host.prototype.RUNTIME_ADOBECEP = "adobe-cep";
Host.prototype.RUNTIME_ADOBEJSX = "adobe-jsx";
Host.prototype.RUNTIME_UNKNOWN = "unknown";
Host.prototype.PLATFORM_WINDOWS = "windows";
Host.prototype.PLATFORM_LINUX = "linux";
Host.prototype.PLATFORM_MACOS = "macos";
Host.prototype.PLATFORM_IOS = "ios";
Host.prototype.PLATFORM_ANDROID = "android";
Host.prototype.PLATFORM_UNKNOWN = "unknown";
Host.prototype.PLATFORM_FREEBSD = "freebsd";
Host.prototype.PLATFORM_SOLARIS = "solaris";
Host.prototype.PLATFORM_LINUX_READYNASOS = "linux readynasos";
Host.prototype.PLATFORM_LINUX_DEBIAN = "linux debian";                                //unsupported
Host.prototype.PLATFORM_LINUX_FEDORA = "linux fedora";                                //unsupported
Host.prototype.PLATFORM_LINUX_GENTOO = "linux gentoo";                                //unsupported
Host.prototype.PLATFORM_LINUX_MANDRAKE = "linux mandrake";                            //unsupported
Host.prototype.PLATFORM_LINUX_SUSE = "linux suse";                                    //unsupported
Host.prototype.PLATFORM_LINUX_REDHAT = "linux red-hat";                               //unsupported
Host.prototype.PLATFORM_LINUX_SLACKWARE = "linux slackware";                          //unsupported
Host.prototype.PLATFORM_LINUX_UBUNTU = "linux ubuntu";
Host.prototype.PLATFORM_LINUX_CHAKRA = "linux chakra";
Host.prototype.PLATFORM_LINUX_IYCC = "linux iycc";
Host.prototype.PLATFORM_LINUX_MINT = "linux mint";
Host.prototype.VERSION_UNKNOWN = "unknown";
Host.prototype.PLATFORM_ARCHITECTURE_X86 = "x86";
Host.prototype.PLATFORM_ARCHITECTURE_X64 = "x64";
Host.prototype.PLATFORM_ARCHITECTURE_ARM = "arm";
Host.prototype.PLATFORM_ARCHITECTURE_ARM64 = "arm64";
Host.prototype.PLATFORM_ARCHITECTURE_UNKNOWN = "unknown";

Host.prototype.ERROR_RUNTIME_NOT_SUPPORTED = "Runtime Not Supported";
Host.prototype.ERROR_RUNTIME = "Runtime Error";
Host.prototype.ERROR_INVALID_HOST_DATA = "Invalid Host Data";

/////////////////////////////////////////////////////////////////////////////////////////////
//
// host Instance
//
/////////////////////////////////////////////////////////////////////////////////////////////
host = new Host();
if (typeof document !== "undefined") {
    document.documentElement.setAttribute("data-host-runtime", host.runtime);
    document.documentElement.setAttribute("data-host-runtime-version", host.runtimeVersion);
    document.documentElement.setAttribute("data-host-platform", host.platform);
    document.documentElement.setAttribute("data-host-platform-version", host.platformVersion);
    document.documentElement.setAttribute("data-host-platform-architecture", host.platformArchitecture);
}

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = host;